/*
Copyright (c) 2014 NicoHood
See the readme for credit to other people.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/

#ifndef __CONSUMERAPI__
#define __CONSUMERAPI__

// to access the HID_SendReport via USBAPI.h and report number
#include "Arduino.h"

//================================================================================
// Consumer
//================================================================================

#define CONSUMER_BIT_NEXT_TRACK		0
#define CONSUMER_BIT_PREV_TRACK		1
#define CONSUMER_BIT_STOP			2
#define CONSUMER_BIT_PLAY_PAUSE		3
#define CONSUMER_BIT_VOLUME_MUTE	4
#define CONSUMER_BIT_VOLUME_UP		5
#define CONSUMER_BIT_VOLUME_DOWN	6

typedef union{
	// every usable Consumer key possible, up to 4 keys presses possible
	uint8_t whole8[];
	uint16_t whole16[];
	uint32_t whole32[];
	struct{
		uint8_t control;
		// bool idle;
		// bool volumeUp;
		// bool volumeDown;
		// bool mute;
		// bool none[4];
		uint8_t none2[3];
		uint8_t none3[4];
	};
} HID_ConsumerControlReport_Data_t;

class Consumer_{
public:
	inline Consumer_(void){
		// empty
	}
	inline void begin(void){
		// release all buttons
		end();
	}
	inline void end(void){
		memset(&_report, 0, sizeof(_report));
		HID_SendReport(HID_REPORTID_CONSUMERCONTROL, &_report, sizeof(_report));
	}

	inline void press(uint8_t bitNum)
	{
		_report.control |= (1 << bitNum);
		HID_SendReport(HID_REPORTID_CONSUMERCONTROL, &_report, sizeof(_report));
	}

	inline void release(uint8_t bitNum)
	{
		_report.control &= ~(1 << bitNum);
		HID_SendReport(HID_REPORTID_CONSUMERCONTROL, &_report, sizeof(_report));
	}

	inline void volumeUp()
	{
		press(CONSUMER_BIT_VOLUME_UP);
		release(CONSUMER_BIT_VOLUME_UP);
	}
	inline void volumeDown()
	{
		press(CONSUMER_BIT_VOLUME_DOWN);
		release(CONSUMER_BIT_VOLUME_DOWN);
	}
	inline void mute()
	{
		press(CONSUMER_BIT_VOLUME_MUTE);
		release(CONSUMER_BIT_VOLUME_MUTE);
	}
	// inline void write(uint16_t m){
	// 	press(m);
	// 	release(m);
	// }
	// inline void press(uint16_t m){
	// 	// search for a free spot
	// 	for (int i = 0; i < sizeof(HID_ConsumerControlReport_Data_t) / 2; i++) {
	// 		if (_report.whole16[i] == 0x00) {
	// 			_report.whole16[i] = m;
	// 			break;
	// 		}
	// 	}
	// 	HID_SendReport(HID_REPORTID_CONSUMERCONTROL, &_report, sizeof(_report));
	// }
	// inline void release(uint16_t m){
	// 	// search and release the keypress
	// 	for (int i = 0; i < sizeof(HID_ConsumerControlReport_Data_t) / 2; i++) {
	// 		if (_report.whole16[i] == m) {
	// 			_report.whole16[i] = 0x00;
	// 			// no break to delete multiple keys
	// 		}
	// 	}
	// 	HID_SendReport(HID_REPORTID_CONSUMERCONTROL, &_report, sizeof(_report));
	// }
	inline void releaseAll(void){
		end();
	}
private:
	HID_ConsumerControlReport_Data_t _report;
};
extern Consumer_ Consumer;

#endif
